///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
// Usage:  hello
//
// Result:
//   Print "Hello World"
//
// @author Albert D'Sanson <adsanson@hawaii.edu>
// @date   11_02_2021
///////////////////////////////////////////////////////////////////////////////

#include<iostream>

using namespace std;

int main(){
    cout<<"Hello World!"<<endl;

    return 0;
}
