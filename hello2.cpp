///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04c - Hello World
//
// Usage:  hello
//
// Result:
//   Print "Hello World"
//
// @author Albert D'Sanson <adsanson@hawaii.edu>
// @date   11_02_2021
///////////////////////////////////////////////////////////////////////////////

#include<iostream>

int main(){

   std::cout<<"Hello World!"<<std::endl;

    return 0;
}
