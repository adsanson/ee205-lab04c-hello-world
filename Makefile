###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04c - Hello World
#
# @file    Makefile
# @version 1.0
#
# @author Albert D'Sanson <adsanson@hawaii.edu>
# @brief  Lab 04c - Hello World - EE 205 - Spr 2021
# @date   11_02_2021
###############################################################################

all: hello1 hello2

hello1: hello1.cpp
	g++ -o hello1 hello1.cpp

hello2: hello2.cpp
	g++ -o hello2 hello2.cpp

clean:
	rm -f *.o hello1 hello2
